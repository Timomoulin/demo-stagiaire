
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stagiaire</title>
    <link rel="stylesheet" href="./bootstrap5/css/bootstrap.css">
    <script src="./bootstrap5/js/bootstrap.js"></script>
</head>
<body>
<?php
    require("menu.php");
?>
<div class="container">
    <div class="row">
    <h1>Page Stagiaire</h1>
    <p>Cette page utilise une fonction du fichier bdd.php dans php/bdd/bdd.php</p>
    <table class="table table-striped table-bordered " >
        <theader>
            <tr class="table-info">
                <td class="p-2 ">#</td>
                <td class="p-2 ">Nom</td>
                <td class="p-2 ">Prenom</td>
                <td class="p-2 ">Email</td>
                <td class="p-2 ">Consulter</td>
            </tr>
        </theader>
        <tbody>
    <?php
    require("./php/bdd/bdd.php");
    $lesStagiaires=getAllStagiaire();
    //var_dump($lesStagiaires);
    foreach($lesStagiaires as $unStagiaire)
    {
        echo("<tr >");
        echo("<td >".$unStagiaire['idStagiaire']."</td>");
        echo("<td >".$unStagiaire['nom']."</td>");
        echo("<td >".$unStagiaire['prenom']."</td>");
        echo("<td >".$unStagiaire['email']."</td>");
        echo("<td > <a class='btn btn-primary' href='./pageConsultationStagiaire.php?id=".$unStagiaire['idStagiaire']."'>Consultation </a></td>");
        echo("</tr>");
    }
    ?>
    </tbody>
    </table>
    </div>
</div>
<?php 
    require("footer.php");
?>

</body>
</html>