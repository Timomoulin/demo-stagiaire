<nav class="navbar navbar-expand-lg navbar-light text-color2 bg-light">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">STAGIAIRE</a>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="./">Accueil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php __DIR__?>./pageStagiaires.php">Les stagiaires</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#">Les formateurs</a>
        </li>
      </ul>
    </div>
  </div>
</nav>