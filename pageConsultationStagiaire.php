
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stagiaire</title>
    <link rel="stylesheet" href="bootstrap5/css/bootstrap.css">
    <script src="bootstrap5/js/bootstrap.js"></script>
</head>
<body>
<?php
    require("menu.php");
?>
<div class="container">
    <div class="row">
    <h1>Fiche stagiaire</h1>
    <p>Le site utilise la BDD.</p>
    <?php
    require("./php/bdd/bdd.php");
    if(isset($_GET["id"]))
    {
        $idStagiaire=$_GET["id"]; //Recup l'id dans l'url

        $idStagiaire=trim($idStagiaire); 
        $idStagiaire=stripslashes($idStagiaire);
        $idStagiaire=htmlspecialchars($idStagiaire);//contre les failles XSS

        $leStagiaire=getStagiaireById($idStagiaire);

        echo("<div>");
        echo("Nom : ".$leStagiaire["nom"]."<br>");
        echo("Prenom : ".$leStagiaire["prenom"]."<br>");
        echo("Email : ".$leStagiaire["email"]."<br>");
        echo("</div>");
        
        $lesEvaluations=getEvaluationByIdStagiaire($idStagiaire);
        echo('<table class="table table-striped table-bordered " >
        <theader>
            <tr class="table-info">
                <td class="p-2 ">Note</td>
                <td class="p-2 ">Date</td>
                <td class="p-2 ">Matiere</td>
            </tr>
        </theader>
        <tbody>');
        //TODO Afficher les evaluations du stagiaires dans un tableau avec les matières
        foreach($lesEvaluations as $uneEvaluation)
        {
            echo("<tr>
            <td>".$uneEvaluation['note']."</td>
            <td>".$uneEvaluation['dateEvaluation']." </td>
            <td>".$uneEvaluation['nomMatiere']."</td>
            </tr>");
        }
        echo("</tbody>
        </table>");
    }
    else{
        echo ("PAS DE ID POUR LE STAGIAIRE");
    }

    ?>
    </div>
</div>
<?php 
    require("footer.php");
?>

</body>
</html>