
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Stagiaire</title>
    <link rel="stylesheet" href="./bootstrap5/css/bootstrap.css">
    <script src="./bootstrap5/js/bootstrap.js"></script>
</head>
<body>
<?php
    require("menu.php");
?>
<div class="container">
    <div class="row">
    <h1>Bienvenue sur le site stagiaire</h1>
    <p>Le site utilise la BDD.</p>
    </div>
</div>
<?php 
    require("footer.php");
?>

</body>
</html>